package com.cnunicom.micro.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicroCouponApplication.class, args);
    }

}
