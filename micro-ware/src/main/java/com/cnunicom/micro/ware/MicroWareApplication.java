package com.cnunicom.micro.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicroWareApplication.class, args);
    }

}
