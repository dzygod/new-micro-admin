package com.cnunicom.member.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicroMemberApplication.class, args);
    }

}
